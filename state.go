package main

import (
	"fmt"
)

const (
	STATUS_ACCESS_DENIED int = iota
	STATUS_EXPECT_RATING
	STATUS_FULLY_RATED
)

var (
	Messages       map[int]string
	AllowedActions map[int]int
)

type State struct {
	UserID             int     `json:"userID"`
	UserName           string  `json:"userName"`
	Phone              string  `json:"phone"`
	AvgRating          float64 `json:"avgRating"`
	NextUserToRate     string  `json:"nextUserToRate"`
	NextUserToRateName string  `json:"nextUserToRateName"`
	NeedCount          int     `json:"needCount"`
	AllCount           int     `json:"allCount"`
	Status             int     `json:"status"`
	ChatID             int64   `json:"chatID"`
	YearDay            int     `json:"yearDay"`
}

func (self *State) getMessageText() string {

	switch self.Status {
	case STATUS_ACCESS_DENIED:
		return "Ваш номер не зарегистрирован в базе сотрудников, участвующих в процедуре оценки"
	case STATUS_EXPECT_RATING:
		return fmt.Sprintf("Осталось оценить: %d из %d \nПоставьте оценку сотруднику %s", self.NeedCount, self.AllCount, self.NextUserToRateName)
	case STATUS_FULLY_RATED:
		var mark string
		if self.AvgRating > 0.1 {
			mark = fmt.Sprintf("%1.1f", self.AvgRating)
		} else {
			mark = "будет доступна, когда наберется статистика"
		}
		return fmt.Sprintf("Сегодня есть оценки у всех %d сотрудников. Спасибо. Ваша средняя оценка %s", self.AllCount, mark)
	}
	return ""
}

func validateState(state *State) {

	if state.AllCount == 0 {
		state.Status = STATUS_ACCESS_DENIED
	} else if state.NeedCount > 0 {
		state.Status = STATUS_EXPECT_RATING
	} else {
		state.Status = STATUS_FULLY_RATED
	}

}

func getActions(status int) []string {

	switch status {
	case STATUS_ACCESS_DENIED:
		return []string{
			ACTION_CONTINUE,
		}

	case STATUS_EXPECT_RATING:
		return []string{
			ACTION_RATE_1,
			ACTION_RATE_2,
			ACTION_RATE_3,
			ACTION_RATE_4,
			ACTION_RATE_5,
			ACTION_RATE_NO_DEALS,
		}
	case STATUS_FULLY_RATED:
		return []string{
			ACTION_CONTINUE,
			ACTION_TOTALS,
		}
	}

	return nil

}
