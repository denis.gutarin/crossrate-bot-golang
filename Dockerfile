FROM golang:1.9 as builder
WORKDIR /go/src/app
COPY . /go/src/app
RUN go-wrapper download
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ./app .

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
RUN mkdir var
COPY --from=builder /go/src/app/app .
CMD ["./app"]  