package main

import (
	"log"
	"os"

	"github.com/boltdb/bolt"
	"gopkg.in/telegram-bot-api.v4"
)

func panicOnErr(err error) {
	if err != nil {
		log.Panic(err)
	}
}

var db *bolt.DB
var dbErr error
var bucket []byte

func main() {

	logFile, logFileError := os.OpenFile("./var/crossrate.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if logFileError != nil {
		log.Fatalf("Error opening file: %v", logFileError)
	}
	defer logFile.Close()
	log.SetOutput(logFile)

	db, dbErr = bolt.Open("./var/storage.db", 0600, nil)
	if dbErr != nil {
		log.Fatal(dbErr)
	}
	defer db.Close()
	bucket = []byte("States")

	bot, botErr := tgbotapi.NewBotAPI("539273260:AAEl8KtEiBU2IcB8XRhGyxGz5uMPjomuQf8") //production - sdvor_crossrate_bot
	// bot, botErr := tgbotapi.NewBotAPI("534113281:AAEJyedK85ob4bruOS6zw-4mMzsg6Sc_KDA") //test - sdvor_test2_bot
	panicOnErr(botErr)

	// bot.Debug = true
	println("Application started...")
	log.Printf("START: Authorized into account  %s \n\n", bot.Self.UserName)

	conf := tgbotapi.NewUpdate(0)
	conf.Timeout = 60

	updates, updatesErr := bot.GetUpdatesChan(conf)
	panicOnErr(updatesErr)

	go notifyWorker(bot)
	for update := range updates {
		go handle(&update, bot)
	}

}
