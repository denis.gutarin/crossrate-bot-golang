package main

import (
	"fmt"
	"log"
	"strings"

	api "gopkg.in/telegram-bot-api.v4"
)

func handle(update *api.Update, bot *api.BotAPI) {

	defer func() {
		if r := recover(); r != nil {
			log.Println("Recovered: ", r)
		}
	}()

	if update.Message == nil {
		return
	}

	log.Printf("USER [%s - %d]\n%s \n\n", update.Message.From.UserName, update.Message.From.ID, update.Message.Text)

	id := update.Message.From.ID
	mess := update.Message.Text

	var state State
	state = getState(id)
	state.UserID = id
	state.UserName = update.Message.From.UserName
	state.ChatID = update.Message.Chat.ID
	if update.Message.Contact != nil {
		state.Phone = update.Message.Contact.PhoneNumber
	}

	var msgToSend api.MessageConfig
	var keyboard api.ReplyKeyboardMarkup
	// state.Phone = "79266922265"
	if state.Phone == "" {

		keyboard = api.NewReplyKeyboard(
			api.NewKeyboardButtonRow(
				api.NewKeyboardButtonContact("ОТПРАВИТЬ НОМЕР"),
			),
		)
		msgToSend = api.NewMessage(update.Message.Chat.ID, "Для продолжения работы отправьте, пожалуйста, ваш номер телефона КНОПКОЙ снизу")
		msgToSend.ReplyMarkup = keyboard

		bot.Send(msgToSend)

	} else {

		var newState *State
		var actionError error
		var list []reportOutputLine
		var newMsgText string

		if strings.ContainsAny(mess, "12345") || mess == ACTION_RATE_NO_DEALS {
			newState, actionError = actionRate(&state, state.NextUserToRate, mess)
			newMsgText = newState.getMessageText()
		} else if mess == ACTION_TOTALS {
			newState = &state
			list, actionError = actionReport(&state)
			newMsgText = "Средние оценки:\n"
			if len(list) == 0 {
				newMsgText = newMsgText + "Пока что статистики не достаточно для сравнения оценок"
			}
			for _, line := range list {
				if line.UserFIO == "" {
					line.UserFIO = line.UserName
				}
				if line.Rating < 3.5 {
					newMsgText = newMsgText + fmt.Sprintf("💩 %s: %1.1f \n", line.UserFIO, line.Rating)
				} else if line.Rating >= 4.8 {
					newMsgText = newMsgText + fmt.Sprintf("🥇 %s: %1.1f \n", line.UserFIO, line.Rating)
				} else {
					newMsgText = newMsgText + fmt.Sprintf("%s: %1.1f \n", line.UserFIO, line.Rating)
				}
			}

		} else {
			newState, actionError = actionRate(&state, "", "")
			newMsgText = newState.getMessageText()
		}

		if actionError != nil {
			log.Panic(actionError)
		}

		if newState != nil {
			saveState(*newState)
		}

		keyboard := getKeyboard(newState.Status)

		log.Printf("BOT => [%s - %d]\n%s \n\n", update.Message.From.UserName, update.Message.From.ID, newMsgText)

		msgToSend := api.NewMessage(update.Message.Chat.ID, newMsgText)
		msgToSend.ReplyMarkup = keyboard

		bot.Send(msgToSend)

	}

}
