package main

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
)

const (
	rateURL   = "https://pas.sdvor.com/api/bot/ZUCR_RATING_SALES/rate"
	reportUrl = "https://pas.sdvor.com/api/bot/ZUCR_RATING_SALES/report"
)

type rateInput struct {
	Phone    string `json:"PHONE"`
	UserName string `json:"UNAME"`
	Rating   int    `json:"RATING"`
}

type rateOutput struct {
	Phone     string  `json:"PHONE"`
	UserName  string  `json:"UNAME"`
	FIO       string  `json:"FIO"`
	NeedCount int     `json:"NEED"`
	AllCount  int     `json:"ALL"`
	AvgRating float64 `json:"SELF_AVG_RATING"`
}

type reportInput struct {
	Phone string `json:"PHONE"`
	Group string `json:"GROUP"`
}

type reportOutputLine struct {
	UserName string  `json:"UNAME"`
	UserFIO  string  `json:"FIO"`
	Rating   float64 `json:"RATING"`
	Count    int     `json:"COUNT"`
}

type reportOutput struct {
	Phone string             `json:"PHONE"`
	List  []reportOutputLine `json:"LIST"`
}

var client *http.Client

type StatusError struct {
	error
	StatusCode int
}

func (it StatusError) Error() string {
	return "Response status is " + strconv.Itoa(it.StatusCode)
}

func call(url string, json []byte) ([]byte, error) {

	if client == nil {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client = &http.Client{Transport: tr}
	}

	response, postError := client.Post(url, "application/json", bytes.NewBuffer(json))
	if postError != nil {
		return nil, postError
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return nil, StatusError{StatusCode: response.StatusCode}
	}

	outJSON, readError := ioutil.ReadAll(response.Body)
	if readError != nil {
		return nil, readError
	}

	return outJSON, nil

}

func callRate(in rateInput) (*rateOutput, error) {

	inJSON, marshalError := json.Marshal(in)
	if marshalError != nil {
		return nil, marshalError
	}

	outJSON, callError := call(rateURL, inJSON)
	if callError != nil {
		return nil, callError
	}

	out := &rateOutput{}
	unmarshalError := json.Unmarshal(outJSON, out)
	if unmarshalError != nil {
		return nil, unmarshalError
	}

	return out, nil

}

func callReport(in reportInput) (*reportOutput, error) {

	inJSON, marshalError := json.Marshal(in)
	if marshalError != nil {
		return nil, marshalError
	}

	outJSON, callError := call(reportUrl, inJSON)
	if callError != nil {
		return nil, callError
	}

	out := &reportOutput{}
	unmarshalError := json.Unmarshal(outJSON, out)
	if unmarshalError != nil {
		return nil, unmarshalError
	}

	return out, nil

}
