package main

import (
	"log"
	"time"

	api "gopkg.in/telegram-bot-api.v4"
)

func notifyWorker(bot *api.BotAPI) {

	for {

		current := time.Now().Year()*1000 + time.Now().YearDay()
		log.Printf("WORKER: Check current date %d ", current)

		if time.Now().UTC().Hour() < 12 {
			log.Printf("WORKER: Will send notification after 12h UTC")
			time.Sleep(time.Hour)
			continue
		}

		notify(bot)
		time.Sleep(time.Hour)

	}

}

func notify(bot *api.BotAPI) {

	defer func() {
		if r := recover(); r != nil {
			log.Println("Recovered: ", r)
		}
	}()

	// current := time.Now().Year()*1000 + time.Now().YearDay()
	// current := 2018999

	states := getAllStates()
	log.Printf("WORKER: Saved states count: %d \n", len(states))

	var newState *State
	var error error
	for _, line := range states {

		newState = &State{}
		log.Printf("WORKER: Start nofity: %d - %s \n", line.UserID, line.UserName)
		newState, error = actionRate(&line, "", "")
		if error != nil {
			continue
		}

		if newState.AllCount <= 0 {
			delState(newState.UserID)
			log.Printf("WORKER: User is not actual now. State deleted  \n\n")
			continue
		}

		if newState.ChatID == 0 {
			log.Printf("WORKER: Cannot find chat ID \n\n")
			continue
		}

		if newState.NeedCount <= 0 {
			log.Printf("WORKER: User has already finished   \n\n")
			continue
		}

		keyboard := getKeyboard(STATUS_FULLY_RATED)

		newMsgText := "Оцените, пожалуйста, сотрудников с которыми Вы работали"
		log.Printf("BOT => [%s - %d]\n%s \n\n", newState.UserName, newState.UserID, newMsgText)

		msgToSend := api.NewMessage(newState.ChatID, newMsgText)
		msgToSend.ReplyMarkup = keyboard

		bot.Send(msgToSend)

	}

}
