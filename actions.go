package main

import (
	"strconv"
	"strings"
	"time"

	api "gopkg.in/telegram-bot-api.v4"
)

const (
	ACTION_CONTINUE      = "Продолжить"
	ACTION_RATE_1        = "1"
	ACTION_RATE_2        = "2"
	ACTION_RATE_3        = "3"
	ACTION_RATE_4        = "4"
	ACTION_RATE_5        = "5"
	ACTION_RATE_NO_DEALS = "Не было сделок"
	ACTION_TOTALS        = "Итоги"
)

func checkFirstLine(text string) bool {
	if strings.ContainsAny(text, "12345") {
		return true
	}
	return false
}

func actionRate(state *State, userName string, rating string) (*State, error) {

	intRating, parseError := strconv.ParseInt(rating, 0, 64)
	if parseError != nil {
		intRating = 0
	}

	if intRating < 1 || intRating > 5 {
		intRating = 0
	}

	out, err := callRate(rateInput{
		Phone:    state.Phone,
		Rating:   int(intRating),
		UserName: userName,
	})
	if err != nil {
		return nil, err
	}

	newState := *state
	newState.NextUserToRate = out.UserName
	newState.NextUserToRateName = out.FIO
	newState.NeedCount = out.NeedCount
	newState.AllCount = out.AllCount
	newState.AvgRating = out.AvgRating

	currentTime := time.Now().UTC()
	newState.YearDay = currentTime.Year()*1000 + currentTime.YearDay()

	validateState(&newState)

	return &newState, nil

}

func getKeyboard(status int) api.ReplyKeyboardMarkup {

	actions := getActions(status)

	firstLine := api.NewKeyboardButtonRow()
	otherButtons := api.NewKeyboardButtonRow()

	for _, action := range actions {
		if checkFirstLine(action) {
			firstLine = append(firstLine, api.NewKeyboardButton(action))
		} else {
			otherButtons = append(otherButtons, api.NewKeyboardButton(action))
		}
	}

	keyboard := api.NewReplyKeyboard()
	if len(firstLine) > 0 {
		keyboard.Keyboard = append(keyboard.Keyboard, firstLine)
	}
	if len(otherButtons) > 0 {
		keyboard.Keyboard = append(keyboard.Keyboard, otherButtons)
	}

	return keyboard

}

func actionReport(state *State) ([]reportOutputLine, error) {

	out, err := callReport(reportInput{
		Phone: state.Phone,
	})
	if err != nil {
		return nil, err
	}

	return out.List, nil
}
