package main

import (
	"encoding/json"
	"strconv"

	"github.com/boltdb/bolt"
)

func saveState(state State) {
	if db == nil {
		return
	}
	db.Update(func(tx *bolt.Tx) error {
		id := strconv.Itoa(state.UserID)
		value, _ := json.Marshal(state)
		buck := tx.Bucket(bucket)
		if buck == nil {
			buck, _ = tx.CreateBucketIfNotExists(bucket)
		}
		buck.Put([]byte(id), value)
		return nil
	})

}

func getState(id int) State {
	state := State{}
	if db == nil {
		return state
	}
	db.View(func(tx *bolt.Tx) error {
		stringID := strconv.Itoa(id)
		buck := tx.Bucket(bucket)
		if buck != nil {
			value := buck.Get([]byte(stringID))
			json.Unmarshal(value, &state)
		}
		return nil
	})
	return state
}

func delState(id int) {
	if db == nil {
		return
	}
	db.Update(func(tx *bolt.Tx) error {
		stringID := strconv.Itoa(id)
		buck := tx.Bucket(bucket)
		if buck == nil {
			buck, _ = tx.CreateBucketIfNotExists(bucket)
		}
		buck.Delete([]byte(stringID))
		return nil
	})
}

func getAllStates() []State {
	if db == nil {
		return nil
	}
	var state State
	var states []State
	states = make([]State, 0, 100)

	db.View(func(tx *bolt.Tx) error {
		buck := tx.Bucket(bucket)
		if buck != nil {
			cursor := buck.Cursor()
			for key, value := cursor.First(); key != nil; key, value = cursor.Next() {
				state = State{}
				json.Unmarshal(value, &state)
				if state.UserID != 0 {
					states = append(states, state)
				}
			}
		}
		return nil
	})
	return states
}
