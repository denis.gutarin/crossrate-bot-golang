#/bin/bash
if [ ! -d "$PWD/var" ]; then
  mkdir var
else
  echo "Directory var already exists"
fi
docker run --rm -it -v $PWD/var:/root/var sdvor_crossrate_bot_golang:latest 